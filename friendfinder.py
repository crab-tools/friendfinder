# friend finder idk when this will be done wtf am i even doing this isn't basic python

# imports
import os
import sys
from time import sleep
import pymongo
from pymongo import MongoClient

# mongodb connect
CONNECTION_STRING = "mongodb+srv://sifas-finder:snwLtXKD2lLZ8uUv@base.k1185vp.mongodb.net/?retryWrites=true&w=majority"
client = MongoClient(CONNECTION_STRING)
print("Connected to MongoDB Database.")
db = client['notespace']
collection = db["SIFAS-codes"]
chk = db.list_collection_names()
if "SIFAS-codes" in chk:
    print("Accepting Entries!")
    sleep(1)
    os.system('cls')
else: 
    print("MongoDB Database is either missing, corrupt, and/or deleted.")
    print("Please report bug. ")

# main code
while True:
    print("IdolBook b.0.1 by FutureRing | SIFAS Friend Code DB | (c)2023\n")
    print("Type 1 for Search, 2 for Addition. Type 0 to exit.")
    try: # inputs
        sel = int(input("Mode: "))
    except ValueError:
        print("Invalid Option")
        sleep(1)
        os.system("cls")
        continue
    if sel == 1: # find acc
        os.system("cls")
        print("FIND ACCOUNT IN DATABASE")
        print("Type 1 for Code Search, 2 for IGN Search. 0 to go back.")
        while True:
            try:
                q = int(input("Search by > "))
            except ValueError: 
                print("Query Cleared.")
                break
            else: 
                if q == 1: # code search
                    query = int(input("Friend Code: "))
                    if len(str(query)) == 9:
                        res = collection.find_one({"Code": query})
                        if res["Code"] == query: 
                            os.system('cls')
                            print("Found!\n")
                            print("Friend Code: " + str(res["Code"]))
                            print("IGN: " + res["IGN"])
                            print("Discord USN: " + res["Discord"])
                            sleep(1.5)
                            try:
                                input("\nPress ENTER to return to Menu... ")
                            except SyntaxError:
                                pass
                            else: 
                                os.system('cls')
                                break
                        else: 
                            print("Friend Code does not exist.")
                            try:
                                input("\nPress ENTER to return to Menu... ")
                            except SyntaxError:
                                pass
                            else: 
                                os.system('cls')
                                break
                    else: 
                        print("Input is not a valid Friend Code.")
                        sleep(1)
                        os.system('cls')
                        continue
                elif q == 2: # IGN search
                    query = input("IGN: ")
                    if len(query) != 0:
                        res = collection.find_one({"IGN": query})
                        if res["IGN"] == query: 
                            os.system('cls')
                            print("Found!\n")
                            print("IGN: " + res["IGN"])
                            print("Friend Code: " + str(res["Code"]))
                            print("Discord USN: " + res["Discord"])
                            sleep(1.5)
                            try:
                                input("\nPress ENTER to return to Menu... ")
                            except SyntaxError:
                                pass
                            else: 
                                os.system('cls')
                                break
                        else: 
                            print("IGN does not exist.")
                            try:
                                input("\nPress ENTER to return to Menu... ")
                            except SyntaxError:
                                pass
                            else: 
                                os.system('cls')
                                break
                elif q == 0:
                    break            
                else: 
                    print("Returning to Main Menu...")
                    sleep(1)
                    os.system('cls')
                    break
    elif sel == 2: # add acc
        os.system("cls")
        print("ADD NEW ACCOUNT TO DATABASE\n")
        while True:
            try: 
                add_n = int(input("Your Friend Code: "))
            except ValueError: 
                print("Invalid Friend Code. Retry.")
                sleep(1)
                os.system('cls')
                continue
            else: 
                if len(str(add_n)) == 9: 
                    chk = collection.count_documents({"Code": add_n})
                    if chk != 1:
                        User = {
                            "Code" : add_n
                        }
                        add_i = input("IGN: ")
                        if len(add_i) != 0: 
                            add_d = input("Discord Username: ")
                            os.system('cls')
                            print("\nFriend Code: " + str(add_n))
                            print("IGN: " + add_i)
                            print("Discord USN: " + add_d)
                            print("\nConfirm Details? y/n")
                            v = input("> ")
                            if v == "y" or "Y": 
                                User["IGN"] = add_i
                                User["Discord"] = add_d
                                collection.insert_many([User])
                                print("\nAdded to Database!")
                                sleep(1)
                                try:
                                    input("Press ENTER to return to Menu... ")
                                except SyntaxError:
                                    pass
                                else: 
                                    os.system('cls')
                                    break
                        else: 
                            print("IGN required!")
                            sleep(1)
                            os.system("cls")
                            continue
                    else:
                        print("Account exists already!")
                        sleep(1.5)
                        os.system("cls")
                        continue
                else: 
                    print("Input is not a valid Friend Code.")
                    sleep(1)
                    os.system('cls')
                    continue
    elif sel == 0: # exit
        os.system('cls')
        print("Thanks for trying out my small program!")
        print("See you next time!")
        sleep(1.5)
        break
    else: 
        print("Invalid Mode.")
        sleep(1)
        os.system('cls')
        continue    